%% This is the MATLAB file created by Dr. Joel A Rosenfeld to support his video on the Nyquist Criterion and Stability of LTI systems.
%If you use this code for your own project, please provide a link to the original video, https://youtu.be/5fISdkNkvVA, and the website https://www.thelearningdock.org

%% Nyquist Contours


%% 3 Zero 1 Pole
v = VideoWriter('threezeroonepole.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-(5+5*sqrt(-1))).*(x-(5-5*sqrt(-1))).*(x-1)./(x-5);



for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-350 350 -0.35 0.35]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 1.3 -0.35 0.35]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 1 Pole 1 Zero
v = VideoWriter('onepoleonezero.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-2)./(x-5);
zerosF = [2;0];
polesF = [5;0];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end



for j = 1:60
   writeVideo(v,getframe(gcf)); 
end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 1.3 -0.35 0.35]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 1.3 -0.35 0.35]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 3 Poles 1 Zero
v = VideoWriter('3pole1zero.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-2)./(x-(5+sqrt(-1)*5))./(x-(5-sqrt(-1)*5))./(x-10);
zerosF = [2;0];
polesF = [5,5,10;5,-5,0];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end



for j = 1:60
   writeVideo(v,getframe(gcf)); 
end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.01 0.01 -0.01 0.01]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.01 0.01 -0.01 0.01]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 2 Poles 1 Zero
v = VideoWriter('twopoleonezero.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-2)./(x-(5+sqrt(-1)*5))./(x-(5-sqrt(-1)*5));
zerosF = [2;0];
polesF = [5,5;5,-5];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end



for j = 1:60
   writeVideo(v,getframe(gcf)); 
end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 0.15 -0.1 0.1]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 0.15 -0.1 0.1]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 1 Zero 
v = VideoWriter('onezero.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-5);
zerosF = [5;0];
%polesF = [5,5;5,-5];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
%plot(polesF(1,:),polesF(2,:),'x');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'bo');
%plot(polesF(1,:),polesF(2,:),'x');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end



for j = 1:60
   writeVideo(v,getframe(gcf)); 
end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-5.5 15.5 -21 21]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");

axis([-5.5 15.5 -21 21]);


writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 1 Poles 
v = VideoWriter('onepole.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) 1./(x-5);
%zerosF = [2;0];
polesF = [5;0];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
%plot(zerosF(1,:),zerosF(2,:),'o');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
%plot(zerosF(1,:),zerosF(2,:),'o');
plot(polesF(1,:),polesF(2,:),'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end



for j = 1:60
   writeVideo(v,getframe(gcf)); 
end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.22 0.1 -0.12 0.12]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'bx');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.22 0.1 -0.12 0.12]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);







%% Contour Integration

v = VideoWriter('riemannsphere.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 1000 400])


t = 0:0.02:2*pi;
t1 = 0:0.02:pi;

theta = 0.001:0.01:pi/2;


for i = 1:length(theta)

[X,Y,Z] = sphere;
X = X/2;
Y = Y/2;
Z = Z/2 + 1/2;


Z1 = sin(t)/1.99 + 1/2;
X1 = cos(t)/1.99;
Y1 = zeros(size(Z1))/1.99;

x = cos(theta(i));
y = tan(theta(i));
z = x^2;
height = 1-x^2;
radius = x*sin(theta(i));

IndexMe = find(Z1 < 1-x^2);
Z1 = Z1(IndexMe);
X1 = X1(IndexMe);
Y1 = Y1(IndexMe);

X2 = radius*cos(t1);
Y2 = -radius*sin(t1);
Z2 = height*ones(size(X2));

X3 = y*cos(t1);
Y3 = -y*sin(t1);
Z3 = zeros(size(X3));

surf(X,Y,Z,'edgecolor','none');
colormap winter;
hold on
plot3(X1,Y1,Z1,'r','LineWidth',3);
hold on
plot3(X2,Y2,Z2,'r','LineWidth',3);
hold on
plot3(X3,Y3,Z3,'r','LineWidth',3);
hold on
plot3(X3,Z3,Z3,'r','LineWidth',3);
hold on
plot3([X3(end),0],[0,0],[0,1],'g','LineWidth',3);
hold on
plot3([X3(1),0],[0,0],[0,1],'g','LineWidth',3);
hold off


grid on;
ylabel("Real");
xlabel("Imag");


axis([-1.6 1.6 -1.6 1.6 0 1.2]);

writeVideo(v,getframe(gcf));

end

surf(X,Y,Z,'edgecolor','none');
colormap winter;
hold on
plot3(X1,Y1,Z1,'r','LineWidth',3);
hold on
plot3(X2,Y2,Z2,'r','LineWidth',3);
hold on
plot3(X3,Y3,Z3,'r','LineWidth',3);
hold on
plot3(X3,Z3,Z3,'r','LineWidth',3);
hold off


grid on;
ylabel("Real");
xlabel("Imag");


axis([-1.6 1.6 -1.6 1.6 0 1.2]);

for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% 2 Poles 1 Zero
v = VideoWriter('contourmapping.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 400 400])

x = -1:0.005:1;
t = 0:0.005:pi;
scaleme = 20;

f1 = @(x) (x-2)./(x-(5+sqrt(-1)*5))./(x-(5-sqrt(-1)*5));
zerosF = [2;0];
polesF = [5,5;5,-5];

for i = 1:length(x)
    
plot(scaleme*zeros(1,i),scaleme*x(1:i),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'o');
plot(polesF(1,:),polesF(2,:),'x');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(scaleme*zeros(1,length(x)),scaleme*x,'b','LineWidth',3);
hold on;
plot(scaleme*sin(t(1:i)),scaleme*cos(t(1:i)),'b','LineWidth',3);
hold on;
plot(zerosF(1,:),zerosF(2,:),'o');
plot(polesF(1,:),polesF(2,:),'x');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5*scaleme 1.5*scaleme -1.2*scaleme 1.2*scaleme]);

writeVideo(v,getframe(gcf));

end


for i = 1:length(x)
    
plot(real(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),...
    imag(f1(scaleme*(zeros(1,i) + sqrt(-1)*x(1:i)))),'b','LineWidth',3);
hold on;
plot(0,0,'x');
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 0.125 -0.1 0.1]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(real(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),...
    imag(f1(scaleme*(zeros(1,length(x)) + sqrt(-1)*x))),'b','LineWidth',3);
hold on;
plot(real(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),...
    imag(f1(scaleme*(sin(t(1:i)) + sqrt(-1)*cos(t(1:i))))),'b','LineWidth',3);
hold on;
plot(0,0,'x');
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.15 0.125 -0.1 0.1]);

writeVideo(v,getframe(gcf));

end




for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);


%% Nyquist Contour

v = VideoWriter('nyquistcontour.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 1010 1010])

x = -1:0.02:1;
t = 0:0.02:pi;


for i = 1:length(x)
    
plot(zeros(1,i),x(1:i),'b','LineWidth',3);
hold off;

grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5 1.5 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

for i = 1:length(t)
plot(zeros(1,length(x)),x,'b','LineWidth',3);
hold on;
plot(sin(t(1:i)),cos(t(1:i)),'b','LineWidth',3);
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5 1.5 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

for i = 1:100
plot(zeros(1,length(x)),exp(i-1)*x,'b','LineWidth',3);
hold on;
plot(exp(i-1)*sin(t),exp(i-1)*cos(t),'b','LineWidth',3);
hold off;
grid on;
xlabel("Real");
ylabel("Imag");


axis([-0.5 (exp(i-1)+0.5) -(exp(i-1) + 0.2) (exp(i-1) + 0.2)]);

writeVideo(v,getframe(gcf));

end

for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

close(v);

%% Draw Funciton

v = VideoWriter('expstable.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 2100 300])

t = 0:0.005:3;

%f = @(x) (x < 1) + 1/2*(x >= 1).*(x < 2).*(cos( pi*(x - 1) ) + 1);
scalefn = @(k) 2*49/pi*(atan( k/10-10 )+pi/2) + 1;

for i = 1:length(t)
    
f = @(x) max(0,1-abs(100*(x-0.5)));
y = f(t(1:i));
plot(t(1:i),y,'b','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 3 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

for i = 1:length(t)
    
f = @(x) 10*sin(10*x).*exp(-3*x);
f2 = @(x) max(0,x-0.5).*f(x-0.5);
y2 = f2(t(1:i));
plot(t,y,'b','LineWidth',3);
hold on
plot(t(1:i),y2,'r','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 3 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

close(v);

%% Draw Function

v = VideoWriter('l2stable.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 2100 300])

t = 0:0.005:3;

%f = @(x) (x < 1) + 1/2*(x >= 1).*(x < 2).*(cos( pi*(x - 1) ) + 1);
scalefn = @(k) 2*49/pi*(atan( k/10-10 )+pi/2) + 1;

for i = 1:length(t)
    
f = @(x) 1/2*sin(100*x)./(x+1).^2;
y = f(t(1:i));
plot(t(1:i),y,'b','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 3 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

for i = 1:length(t)
    
f = @(x) cos(100*x)./(x+1).^2;
y2 = f(t(1:i));
plot(t,y,'b','LineWidth',3);
hold on
plot(t(1:i),y2,'r','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 3 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

close(v);
%% Draw Funciton

v = VideoWriter('boundedinput.avi');
open(v);
figure('Renderer', 'painters', 'Position', [10 10 2100 300])

t = 0:0.005:5;

%f = @(x) (x < 1) + 1/2*(x >= 1).*(x < 2).*(cos( pi*(x - 1) ) + 1);
scalefn = @(k) 2*49/pi*(atan( k/10-10 )+pi/2) + 1;

for i = 1:length(t)
    
f = @(x) 1/4*sin(100*x);
y = f(t(1:i));
plot(t(1:i),y,'b','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 5 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

for j = 1:60
   writeVideo(v,getframe(gcf)); 
end

for i = 1:length(t)
    
f = @(x) cos(100*x);
y2 = f(t(1:i));
plot(t,y,'b','LineWidth',3);
hold on
plot(t(1:i),y2,'r','LineWidth',3);
hold off;

xlabel("t");
ylabel("y");


axis([0 5 -1.2 1.2]);

writeVideo(v,getframe(gcf));

end

close(v);
